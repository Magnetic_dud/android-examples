package it.dandandin.formstuff;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.Toast;
import android.widget.ToggleButton;

public class HelloFormStuffActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        //fare qualcosa col bottone
        final Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		//fai qualcosa al clic
        		Toast.makeText(HelloFormStuffActivity.this, "Ciaociao", Toast.LENGTH_SHORT).show();
        	}
        });
        
        //fare qualcosa con la casella di testo
        final EditText edittext = (EditText) findViewById(R.id.edittext);
        edittext.setOnKeyListener(new OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				//if thie event is a key-down event on the enter button
				if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
						(keyCode == KeyEvent.KEYCODE_ENTER)) {
					// fai qualcosa al key press
					Toast.makeText(HelloFormStuffActivity.this, edittext.getText(), Toast.LENGTH_SHORT).show();
					return true;
				}
				return false;
			}
		});
        
        //fare qualcosa con la checkbox
        final CheckBox checkbox = (CheckBox) findViewById(R.id.checkbox);
        checkbox.setOnClickListener(new OnClickListener() {		
			@Override
			public void onClick(View v) {
				//fai qualcosa a ogni clic
				if (((CheckBox) v).isChecked())
					Toast.makeText(HelloFormStuffActivity.this, "Selected", Toast.LENGTH_SHORT).show();
				else
					Toast.makeText(HelloFormStuffActivity.this, "Deselezionato", Toast.LENGTH_SHORT).show();
				
			}
		});
        
        final RadioButton radio_red = (RadioButton) findViewById(R.id.radio_red);
        final RadioButton radio_blue = (RadioButton) findViewById(R.id.radio_blue);
        radio_red.setOnClickListener(radio_listener);
        radio_blue.setOnClickListener(radio_listener);
        
        //togglebutton
        final ToggleButton togglebutton = (ToggleButton) findViewById(R.id.togglebutton);
        togglebutton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//fai qualcosa al click
				if (togglebutton.isChecked())
					Toast.makeText(HelloFormStuffActivity.this, "Attivato", Toast.LENGTH_SHORT).show();
				else
					Toast.makeText(HelloFormStuffActivity.this, "Disattivato", Toast.LENGTH_SHORT).show();
				
			}
		});
        
        //ratingbar
        final RatingBar ratingbar = (RatingBar) findViewById(R.id.ratingbar);
        ratingbar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
			@Override
			public void onRatingChanged(RatingBar ratingBar, float rating,
					boolean fromUser) {
				Toast.makeText(HelloFormStuffActivity.this, "Voto: " + rating, Toast.LENGTH_SHORT).show();
				
			}
		});
    }
    
    private OnClickListener radio_listener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			//fai qualcosa al click sul radio button
			RadioButton rb = (RadioButton) v;
			Toast.makeText(HelloFormStuffActivity.this, rb.getText(), Toast.LENGTH_SHORT).show();
		}
	};
}